# OpenML dataset: bank-marketing

https://www.openml.org/d/1558

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Paulo Cortez, Sérgio Moro   
**Source**: [original] (http://www.openml.org/d/1461) - UCI    
**Please cite**: S. Moro, R. Laureano and P. Cortez. Using Data Mining for Bank Direct Marketing: An Application of the CRISP-DM Methodology. In P. Novais et al. (Eds.), Proceedings of the European Simulation and Modelling Conference - ESM'2011, pp. 117-121, Guimarães, Portugal, October, 2011. EUROSIS.       

* Dataset:   
Reduced version (10 % of the examples) of bank-marketing dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1558) of an [OpenML dataset](https://www.openml.org/d/1558). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1558/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1558/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1558/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

